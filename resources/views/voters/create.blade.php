@extends('layout.daftar')

@section('content')


<div class="row mt-5 mb-5">
    <div class="col">

    @if($errors->any() )
        <p class="alert alert-danger">Please check your input</p>
    @endif

    <form action="" method="POST" enctype="multipart/form-data">
    @csrf

    <a href="">Back to Book Listing</a>
    <h4>Edit Book</h4>

    <div class="mb-3">
        <label for="title" class="form-label">Title</label>
        <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="">

        @error('title')
        <div class="invalid-feedback">{{$message}}</div>
        @enderror
    </div>
    
    <div class="mb-3">
        <label for="price" class="form-label">Price</label>
        <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="">
        @error('price')
        <div class="invalid-feedback">{{$message}}</div>
        @enderror
    </div>

    <div class="mb-3">
        <label for="synopsis" class="form-label">Synopsis</label>
        <textarea name="synopsis" id="synopsis" rows="10" class="form-control @error('synopsis') is-invalid @enderror"></textarea>
        @error('synopsis')
        <div class="invalid-feedback">{{$message}}</div>
        @enderror
    </div>
    
    <div class="mb-3">
        <label for="title" class="form-label">Image</label>
        <input type="file" class="form-control @error('image_path') is-invalid @enderror" id="image_path" name="image_path" value="">

        @error('image_path')
        <div class="invalid-feedback">{{$message}}</div>
        @enderror
    </div>

    <button class="btn btn-primary">SAVE</button>
    </div>

    </form>
</div>

@endsection